﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace CargaMasivaClientes
{
    public class AccesoDatos
    {
        public void ConsultarPerfiles()
        {

        }

        public List<tipos_de_usuario> ListarTiposUsuarios()
        {
            List<tipos_de_usuario> response = new List<tipos_de_usuario>();
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = "select* from tipos_de_usuario";
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        response.Add(new tipos_de_usuario { id = Convert.ToInt32(reader["id"]), descripcion = reader["descripcion"].ToString() });
                    }
                }
            }
            return response;
        }

        public List<perfil> ListarPerfiles()
        {
            List<perfil> response = new List<perfil>();
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = "select* from perfil where estado = 'ALT';";
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        response.Add(new perfil
                        {
                            abreviatura_empresa = reader["abreviatura_empresa"] == DBNull.Value ? string.Empty : reader["abreviatura_empresa"].ToString(),
                            comentario = reader["comentario"] == DBNull.Value ? string.Empty : reader["comentario"].ToString(),
                            dashboard_home = reader["dashboard_home"] == DBNull.Value ? string.Empty : reader["dashboard_home"].ToString(),
                            des_perfil = reader["des_perfil"] == DBNull.Value ? string.Empty : reader["des_perfil"].ToString(),
                            estado = reader["estado"] == DBNull.Value ? string.Empty : reader["estado"].ToString(),
                            id_negocio = reader["id_negocio"] == DBNull.Value ? 0 : Convert.ToInt32(reader["id_negocio"]),
                            id_perfil = reader["id_perfil"] == DBNull.Value ? string.Empty : reader["id_perfil"].ToString(),
                            tipo_asignacion = reader["tipo_asignacion"] == DBNull.Value ? 0 : Convert.ToInt32(reader["tipo_asignacion"])
                        });
                    }
                }
            }
            return response;
        }

        public void RegistrarUsuarioWebKallpa(string idUserWeb, string rucCliente, int idPerfil)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.kallpaFlota))
            {
                sqlcnn.Open();
                using (SqlCommand cmd = new SqlCommand("USP_INSERTAR_USUARIO_WEB", sqlcnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@IDUSUARIOWEB", SqlDbType.VarChar).Value = idUserWeb;
                    cmd.Parameters.Add("@CUSTIDSS", SqlDbType.VarChar).Value = string.IsNullOrEmpty(rucCliente) ? (object)DBNull.Value : rucCliente;
                    cmd.Parameters.Add("@IDPERFIL", SqlDbType.Int).Value = idPerfil;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RegistrarUsuarioWebContratoKallpa(string idUserWeb, string nroContrato, bool verContrato)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.kallpaFlota))
            {
                sqlcnn.Open();
                using (SqlCommand cmd = new SqlCommand("USP_INSERTAR_USUARIO_WEB_CONTRATO", sqlcnn))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@IDUSUARIOWEB", SqlDbType.VarChar).Value = idUserWeb;
                    cmd.Parameters.Add("@NUMCONTRATO", SqlDbType.VarChar).Value = nroContrato;
                    cmd.Parameters.Add("@VER_CONTRATO", SqlDbType.Bit).Value = verContrato;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void EliminarUsuarioWebKallpsa(string idUserWeb)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.kallpaFlota))
            {
                sqlcnn.Open();
                //string query = string.Format("update PC_USUARIO_WEB set estado = 0 where IDUSUARIOWEB = '{0}'", idUserWeb);
                string query = string.Format("delete from PC_USUARIO_WEB where IDUSUARIOWEB = '{0}'", idUserWeb);
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void EliminarUsuarioCliente(string idUserWeb, string empresa)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = string.Format("delete from usuario_clientes where id_usuario = '{0}' and empresa = '{1}';", idUserWeb, empresa);
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RegistrarUsuarioClientes(string idUsuario, string idCliente, string empresa, int canal, int sector)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = "insert into usuario_clientes (id_usuario, id_cliente, empresa, canal, sector) values (@id_usuario, @id_cliente, @empresa, @canal, @sector);";
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.VarChar).Value = idUsuario;
                    cmd.Parameters.Add("@id_cliente", SqlDbType.VarChar).Value = idCliente;
                    cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = empresa;
                    cmd.Parameters.Add("@canal", SqlDbType.TinyInt).Value = canal;
                    cmd.Parameters.Add("@sector", SqlDbType.TinyInt).Value = sector;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RegistrarUsuarioDespachos(string idUsuario, string idCliente, string empresa, int canal, int sector)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = string.Format("insert into usuario_despacho (id_usuario, codcli, empresa, canal, sector, estado, fecha_creacion, usuario_creacion) values (@id_usuario, @id_cliente, @empresa, @canal, @sector, 1, GETDATE(), 'ProcesoMasivo');");
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.VarChar).Value = idUsuario;
                    cmd.Parameters.Add("@id_cliente", SqlDbType.VarChar).Value = idCliente;
                    cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = empresa;
                    cmd.Parameters.Add("@canal", SqlDbType.TinyInt).Value = canal;
                    cmd.Parameters.Add("@sector", SqlDbType.TinyInt).Value = sector;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void EliminarUsuarioDespacho(string idUserWeb, string empresa)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = string.Format("update usuario_despacho set estado = 0, fecha_modificacion = GETDATE(), usuario_modificacion = 'ProcesoMasivo' where id_usuario = '{0}' and empresa = '{1}';", idUserWeb, empresa);
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void EliminarUsuarioContratos(string idUserWeb, string empresa, string codCliente)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = string.Format("update usuario_contratos set estado = 0, usuario_modificacion = 'ProcesoMasivo', fecha_modificacion = '{1}' where id_usuario='{0}' and empresa = '{2}' and codigoClienteJde = '{3}'", idUserWeb, DateTime.Now.ToString("yyyy-MM-dd hh:mm:dd"), empresa, codCliente);
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandTimeout = 0;
                    cmd.CommandType = CommandType.Text;
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public void RegistrarUsuarioContratos(string idUsuario, string idCliente, string empresa, string contrato)
        {
            using (SqlConnection sqlcnn = new SqlConnection(General.conexion))
            {
                sqlcnn.Open();
                string query = string.Format("insert into usuario_contratos (id_usuario, empresa, contrato, codigoClienteJde, estado, fecha_creacion, usuario_creacion) values (@id_usuario, @empresa, @contrato, @id_cliente,  1, '{0}', 'ProcesoMasivo');", DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss"));
                using (SqlCommand cmd = new SqlCommand(query, sqlcnn))
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandTimeout = 0;
                    cmd.Parameters.Add("@id_usuario", SqlDbType.VarChar).Value = idUsuario;
                    cmd.Parameters.Add("@id_cliente", SqlDbType.VarChar).Value = idCliente;
                    cmd.Parameters.Add("@empresa", SqlDbType.Char).Value = empresa;
                    cmd.Parameters.Add("@contrato", SqlDbType.TinyInt).Value = contrato;
                    cmd.ExecuteNonQuery();
                }
            }
        }
    }
}
