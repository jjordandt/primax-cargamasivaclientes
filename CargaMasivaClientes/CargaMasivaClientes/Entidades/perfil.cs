﻿namespace CargaMasivaClientes
{
    public class perfil
    {
        public perfil()
        {
        }
        public string id_perfil { get; set; }
        public string des_perfil { get; set; }
        public string estado { get; set; }
        public string comentario { get; set; }
        public int? tipo_asignacion { get; set; }
        public int? id_negocio { get; set; }
        public string abreviatura_empresa { get; set; }
        public string dashboard_home { get; set; }
    }
}
