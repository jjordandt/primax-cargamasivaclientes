﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargaMasivaClientes
{
    public static class General
    {
        public static string conexion = ConfigurationManager.AppSettings["default"];
        public static string kallpaFlota = ConfigurationManager.AppSettings["kallpaFlotas"];

        public static List<Lista> Canales()
        {
            List<Lista> canales = new List<Lista>();
            canales.Add(new Lista { Codigo = 10, Valor = "Distribuidor" });
            canales.Add(new Lista { Codigo = 20, Valor = "Autoconsumo" });
            return canales;
        }

        public static List<Lista> Sectores()
        {
            List<Lista> sectores = new List<Lista>();
            sectores.Add(new Lista { Codigo = 10, Valor = "Comb. Líquidos Livianos" });
            sectores.Add(new Lista { Codigo = 20, Valor = "Comb. Líquidos Pesados" });
            sectores.Add(new Lista { Codigo = 30, Valor = "GLP Granel" });
            sectores.Add(new Lista { Codigo = 40, Valor = "GLP Envasado" });
            sectores.Add(new Lista { Codigo = 50, Valor = "Descripcion Pendiente" });
            sectores.Add(new Lista { Codigo = 60, Valor = "Descripcion Pendiente" });
            sectores.Add(new Lista { Codigo = 70, Valor = "Descripcion Pendiente" });
            sectores.Add(new Lista { Codigo = 80, Valor = "Descripcion Pendiente" });
            return sectores;
        }
    }

    public class Lista
    {
        public int Codigo { get; set; }
        public string Valor { get; set; }
    }
}
