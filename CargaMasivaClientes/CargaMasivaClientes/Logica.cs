﻿using ExcelDataReader;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace CargaMasivaClientes
{
    public class Logica
    {
        //List<Lista> Canales = General.Canales();
        //List<Lista> Sectores = General.Sectores();
        List<tipos_de_usuario> ListaTiposUsuarios = new List<tipos_de_usuario>();
        List<perfil> ListaPerfiles = new List<perfil>();

        public Logica()
        {
            ListaTiposUsuarios = new AccesoDatos().ListarTiposUsuarios();
            ListaPerfiles = new AccesoDatos().ListarPerfiles();
        }

        public void ProcesarArchivos(string ruta)
        {
            try
            {
                DirectoryInfo di = new DirectoryInfo(ruta);
                FileInfo[] files = di.GetFiles("*.xlsx");
                if (files.Length > 0)
                {
                    for (int i = 0; i < files.Length; i++)
                    {
                        ProcesarArchivo(files[i]);
                    }
                }
                else
                {
                    Console.WriteLine(string.Format("No existen archivos Excel en la ruta: '{0}'.", ruta));
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        void ProcesarArchivo(FileInfo file)
        {
            Console.WriteLine(string.Format("\nArchivo '{0}': Inicio - {1}", file.Name, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")));
            try
            {
                DataSet datos = new DataSet();
                using (FileStream stream = File.Open(file.FullName, FileMode.Open, FileAccess.ReadWrite))
                {
                    IExcelDataReader reader = CreateReader(file, stream);
                    datos = ConvertDataSet(reader);
                    DataTable dt = new DataTable();
                    datos.Tables[0].Columns[2].ColumnName = "ColUsuario";
                    datos.Tables[0].Columns[10].ColumnName = "ColNroCliSAP";
                    if (datos.Tables.Count > 0 && datos.Tables.Count <= 3)
                    {
                        for (int i = 0; i < datos.Tables.Count; i++)
                        {
                            dt = datos.Tables[i];
                            if (i == 0)
                            {
                                ProcesarPestaniaUsuarios(dt);
                            }
                            else if (i == 1)
                            {
                                ProcesarPestaniaDespachos(dt, datos.Tables[0]);
                            }
                            else if (i == 2)
                            {
                                ProcesarPestaniaContratos(dt, datos.Tables[0]);
                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine("No se puede procesar el archivo {0}, ya que no contiene pestañas o sobrepasa el maximo (3) de pestañas.");
                    }
                }
                datos.Clear();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.WriteLine(string.Format("Archivo {0}: Fin - {1}\n", file.Name, DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss")));
        }

        IExcelDataReader CreateReader(FileInfo file, FileStream fs)
        {
            IExcelDataReader reader = null;
            if (file.Name.ToUpper().EndsWith(".XLSX"))
                reader = ExcelReaderFactory.CreateOpenXmlReader(fs);
            else if (file.Name.ToUpper().EndsWith(".XLS"))
                reader = ExcelReaderFactory.CreateBinaryReader(fs);
            else
                throw new Exception(string.Format("La extensión del archivo {0} no esta permitida.", file.Name));
            return reader;
        }

        DataSet ConvertDataSet(IExcelDataReader reader)
        {
            ExcelDataSetConfiguration conf = new ExcelDataSetConfiguration
            {
                ConfigureDataTable = _ => new ExcelDataTableConfiguration
                {
                    UseHeaderRow = true
                }
            };
            return reader.AsDataSet(conf); ;
        }

        void ProcesarPestaniaUsuarios(DataTable dt)
        {
            if (dt.Rows.Count > 0)
            {
                Console.WriteLine(string.Format("\t{1} - Procesando pestaña '{0}'", dt.TableName, DateTime.Now));
                int idTipoUsuario = 0;
                string idPerfil = string.Empty;
                perfil perfil = new perfil();
                tipos_de_usuario tipoUsuario = new tipos_de_usuario();
                DataRow dr = null;
                string conceptoPerfil = string.Empty, idUsuario = string.Empty, empresa = string.Empty, canal = string.Empty, sector = string.Empty, idClienteSAP = string.Empty;
                int _canal = 0, _sector = 0, contador = 1;
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    dr = dt.Rows[r];
                    idClienteSAP = dr[10].ToString().Trim();
                    if (!idClienteSAP.ToLower().Contains("n/a") && !string.IsNullOrEmpty(idClienteSAP))
                    {
                        idUsuario = dr[2].ToString().Trim();
                        empresa = dr[5].ToString().Trim();
                        canal = dr[6].ToString().Trim();
                        sector = dr[7].ToString().Trim();
                        _canal = string.IsNullOrEmpty(canal) ? 0 : Convert.ToInt32(canal);
                        _sector = string.IsNullOrEmpty(sector) ? 0 : Convert.ToInt32(sector);
                        tipoUsuario = ListaTiposUsuarios.Where(x => x.descripcion.Contains(dr[1].ToString())).FirstOrDefault();
                        if (tipoUsuario != null)
                        {
                            idTipoUsuario = tipoUsuario.id;
                        }
                        else
                        {
                            Console.WriteLine(string.Format("No se encontro el tipo de usuario '{0}', en la fila {1}", dr[0], contador));
                        }
                        perfil = ListaPerfiles.Where(x => x.des_perfil.Contains(dr[11].ToString())).FirstOrDefault();
                        if (perfil != null)
                        {
                            idPerfil = perfil.id_perfil;
                            conceptoPerfil = idPerfil.Split('-')[1];
                        }
                        else
                        {
                            Console.WriteLine(string.Format("No se encontro el perfil '{0}', en la fila {1}", dr[11], contador));
                            //continue;
                        }

                        if (!string.IsNullOrEmpty(conceptoPerfil) && idTipoUsuario > 0)
                        {
                            new AccesoDatos().EliminarUsuarioWebKallpsa(idUsuario);
                            if (idTipoUsuario == 3 && conceptoPerfil.Equals("ADM"))
                            {
                                new AccesoDatos().RegistrarUsuarioWebKallpa(idUsuario, string.Empty, 110);
                            }
                            else if ((idTipoUsuario == 3 && !conceptoPerfil.Equals("ADM")) || (idTipoUsuario == 1))
                            {
                                new AccesoDatos().RegistrarUsuarioWebKallpa(idUsuario, string.Empty, 111);
                            }
                        }

                        new AccesoDatos().EliminarUsuarioCliente(idUsuario, empresa);
                        new AccesoDatos().RegistrarUsuarioClientes(idUsuario, idClienteSAP, empresa, _canal, _sector);
                    }
                    else
                    {
                        Console.WriteLine(string.Format("\t\tLa fila {0} no contiene numero de cliente SAP.", contador));
                    }
                    contador++;
                }
            }
            else
            {
                Console.WriteLine(string.Format("\tLa pestaña {0} no contiene datos.", dt.TableName));
            }
        }

        void ProcesarPestaniaDespachos(DataTable dt, DataTable p1)
        {
            if (dt.Rows.Count > 0)
            {
                Console.WriteLine(string.Format("\t{1} - Procesando pestaña '{0}'", dt.TableName, DateTime.Now));
                DataRow dr = null;
                string idUsuario = string.Empty, empresa = string.Empty, canal = string.Empty, sector = string.Empty, idClienteSAP = string.Empty;
                int _canal = 0, _sector = 0, contador = 1;
                DataRow[] filtroUsuarios = null;
                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    dr = dt.Rows[r];
                    idClienteSAP = dr[1].ToString().Trim();
                    if (!idClienteSAP.ToLower().Contains("n/a") && !string.IsNullOrEmpty(idClienteSAP))
                    {
                        idUsuario = dr[0].ToString().Trim();
                        filtroUsuarios = p1.Select(string.Format("[ColUsuario]='{0}' and [ColNroCliSAP]='{1}'", idUsuario, idClienteSAP));
                        if (filtroUsuarios.Length > 0)
                        {
                            empresa = filtroUsuarios[0][5].ToString().Trim();
                            canal = filtroUsuarios[0][6].ToString().Trim();
                            sector = filtroUsuarios[0][7].ToString().Trim();
                            _canal = string.IsNullOrEmpty(canal) ? 0 : Convert.ToInt32(canal);
                            _sector = string.IsNullOrEmpty(sector) ? 0 : Convert.ToInt32(sector);

                            new AccesoDatos().EliminarUsuarioDespacho(idUsuario, empresa);
                            new AccesoDatos().RegistrarUsuarioDespachos(idUsuario, idClienteSAP, empresa, _canal, _sector);
                        }
                        else
                        {
                            Console.WriteLine(string.Format("\t\tEl usuario {0} con numero SAP '{1}' no se encuentra en la pestaña {2}.", idUsuario, idClienteSAP, p1.TableName));
                        }
                    }
                    else
                    {
                        Console.WriteLine(string.Format("\t\tLa fila {0} no contiene numero de cliente SAP.", contador));
                    }
                    contador++;
                }
            }
            else
            {
                Console.WriteLine(string.Format("\tLa pestaña {0} no contiene datos.", dt.TableName));
            }
        }

        void ProcesarPestaniaContratos(DataTable dt, DataTable p1)
        {
            if (dt.Rows.Count > 0)
            {
                Console.WriteLine(string.Format("\t{1} - Procesando pestaña '{0}'", dt.TableName, DateTime.Now));
                int idTipoUsuario = 0;
                string idPerfil = string.Empty;
                perfil perfil = new perfil();
                tipos_de_usuario tipoUsuario = new tipos_de_usuario();
                DataRow dr = null;
                string conceptoPerfil = string.Empty, idUsuario = string.Empty, empresa = string.Empty, canal = string.Empty, sector = string.Empty, idClienteSAP = string.Empty, contrato = string.Empty;
                int _canal = 0, _sector = 0, contador = 1;
                DataRow[] filtroUsuario = null;

                for (int r = 0; r < dt.Rows.Count; r++)
                {
                    dr = dt.Rows[r];
                    idClienteSAP = dr[1].ToString().Trim();
                    if (!idClienteSAP.ToLower().Contains("n/a") && !string.IsNullOrEmpty(idClienteSAP))
                    {
                        idUsuario = dr[0].ToString().Trim();
                        filtroUsuario = p1.Select(string.Format("[ColUsuario]='{0}' and [ColNroCliSAP] = '{1}'", idUsuario, idClienteSAP));
                        if (filtroUsuario.Length > 0)
                        {
                            empresa = filtroUsuario[0][4].ToString().Trim();
                            canal = filtroUsuario[0][5].ToString().Trim();
                            sector = filtroUsuario[0][6].ToString().Trim();
                            contrato = dr[2].ToString().Trim();
                            _canal = string.IsNullOrEmpty(canal) ? 0 : Convert.ToInt32(canal);
                            _sector = string.IsNullOrEmpty(sector) ? 0 : Convert.ToInt32(sector);
                            tipoUsuario = ListaTiposUsuarios.Where(x => x.descripcion.Contains(filtroUsuario[0][0].ToString())).FirstOrDefault();
                            if (tipoUsuario != null)
                            {
                                idTipoUsuario = tipoUsuario.id;
                            }
                            else
                            {
                                Console.WriteLine(string.Format("No se encontro el tipo de usuario '{0}', en la fila {1}", idUsuario, contador));
                            }
                            perfil = ListaPerfiles.Where(x => x.des_perfil.Contains(filtroUsuario[0][8].ToString())).FirstOrDefault();
                            if (perfil != null)
                            {
                                idPerfil = perfil.id_perfil;
                                conceptoPerfil = idPerfil.Split('-')[1];
                            }
                            else
                            {
                                Console.WriteLine(string.Format("No se encontro el perfil '{0}', en la fila {1}", dr[8], contador));
                            }

                            new AccesoDatos().EliminarUsuarioContratos(idUsuario, empresa, idClienteSAP);
                            new AccesoDatos().RegistrarUsuarioContratos(idUsuario, idClienteSAP, empresa, contrato);
                            if ((idTipoUsuario == 3 && conceptoPerfil.Equals("ADM")) || (idTipoUsuario == 1))
                            {
                                new AccesoDatos().RegistrarUsuarioWebContratoKallpa(idUsuario, contrato, true);
                            }
                        }
                        else
                        {
                            Console.WriteLine(string.Format("\t\tEl usuario {0} con numero SAP '{1}' no se encuentra en la pestaña {2}.", idUsuario, idClienteSAP, p1.TableName));
                        }
                    }
                    else
                    {
                        Console.WriteLine(string.Format("\t\tLa fila {0} no contiene numero de cliente SAP.", contador));
                    }
                    contador++;
                }
            }
            else
            {
                Console.WriteLine(string.Format("\tLa pestaña {0} no contiene datos.", dt.TableName));
            }
        }
    }
}
