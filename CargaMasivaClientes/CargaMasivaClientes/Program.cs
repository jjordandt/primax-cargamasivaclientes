﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CargaMasivaClientes
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(".::CARGA MASIVA DE CLIENTES PS + PORTAL::.");
            Console.Write("Ruta de los archivos excel: ");
            string path = Console.ReadLine();
            new Logica().ProcesarArchivos(path);
            Console.WriteLine("Presionar cualquier tecla para salir de la consola...");
            Console.ReadKey();
        }
    }
}
